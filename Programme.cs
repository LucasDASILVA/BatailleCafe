﻿using BatailleCafe.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Drawing;
using Console = Colorful.Console;
using System.Threading.Tasks;

namespace BatailleCafe
{


    class Programme
    {
        public static string DEBUG()
        {
            Console.WriteLine("-----------------------------------------");
            Console.Write("Ordre a envoyer : ");
            string ordre = Console.ReadLine();
            Console.WriteLine("-----------------------------------------");
            return ordre;
        }

        static void Main()
        {
            const String IP = "127.0.0.1";//"51.91.120.237" ou 172.16.0.88 (machine locale IUT)
            const int PORT = 1213;
            String trameEncodee;
            Socket socketClient = null;
            trameEncodee = FP1_CONNEXION_SERVEUR.Connexion_Serveur(IP,ref socketClient, PORT);
            
            // simulation de reception de trame
            string ScabbIsland = "3:9:71:69:65:65:65:65:65:73|2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|2:8:7:13:14:3:9:6:12:78|6:12:3:1:9:6:12:35:33:41|71:77:6:4:12:39:37:36:36:44|";
            string PhattIsland = "67:69:69:69:69:69:69:69:69:73|74:3:9:7:5:13:3:1:9:74|74:2:8:7:5:13:6:4:12:74|74:6:12:7:9:7:13:3:9:74|74:3:9:11:6:13:7:4:8:74|74:6:12:6:13:11:3:13:14:74|74:7:13:7:13:10:10:3:9:74|74:3:9:11:7:12:14:2:8:74|74:6:12:6:13:7:13:6:12:74|70:69:69:69:69:69:69:69:69:76|";
            string TrameException = "11:15:11:79:79:79:79:79:79:79|6:5:12:79:79:79:79:79:79:79|79:79:79:79:79:79:79:79:79:79|3:5:9:79:79:79:79:79:79:79|14:15:14:79:79:79:79:79:79:79|79:79:79:79:79:79:79:79:79:79|3:13:79:7:9:79:79:79:79:79|10:15:79:15:10:79:79:79:79:79|6:13:79:7:12:79:79:79:79:79|79:79:79:79:79:79:79:79:79:79|";
            Carte carte = FP2_DECODER_CARTE.DecoderCarte(trameEncodee);
            FP3_AFFICHER_CARTE.AfficherCarte(carte,0);
            FP4_DEROULER_PARTIE.DeroulerPartie(carte, socketClient);
            socketClient.Close();
            Console.ReadKey();

        }
    }
}
