﻿using BatailleCafe.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe
{
    static class FP1_CONNEXION_SERVEUR
    {
        /* FS1.1 : INITIALISATION_SOCKET
         * Initialisation de la socket et connexion au serveur fournissant la trame codée.
         * Entrées : SocketClient, une référence d'instance d'une classe Socket; PORT, un entier constant;
         *           IP, une chaîne constante.
         * Sortie : référence de SocketClient (donc passage par adresse)
         */
        private static void Initialisation_Socket(String ip,ref Socket socketClient,int port)
        {
            socketClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                socketClient.Connect(ip, port);
            }
            catch(Exception Erreur)
            {
                Console.WriteLine(Erreur.ToString());
                Console.ReadKey();
            }
        }

        /* FS.1.2 RECEPTION_CARTE
         * Reçoit du serveur une trame représentant la carte et change son encodage en ASCII.
         * Entrée : socketClient, une d'une classe Socket
         * Sortie : trameEncodee une chaîne;
         */
        private static String Reception_Carte(Socket socketClient)
        {
            byte[] donnees = new byte[300];
            String trameEncodee;
            try
            {
                socketClient.Receive(donnees);
            }
            catch (Exception Erreur)
            {
                Console.WriteLine(Erreur.ToString());
                Console.ReadKey();
            }
            trameEncodee = Encoding.ASCII.GetString(donnees);
            //socketClient.Shutdown(SocketShutdown.Receive);
            return trameEncodee;
        }

        /* FP.1 CONNEXION_SERVEUR
         * Se connecte à un serveur avec une IP et un PORT et reçoit la carte codée envoyée par le serveur.
         * Entrées : SocketClient, une référence d'instance d'une classe Socket; PORT, un entier constant;
         *           IP, une chaîne constante.
         * Sortie : référence de SocketClient (donc passage par adresse); trameEncodee une chaine.
         */
        public static String Connexion_Serveur(String ip,ref Socket socketClient, int port)
        {
            String trameEncodee = "";
            Initialisation_Socket(ip,ref socketClient, port);
            trameEncodee = Reception_Carte(socketClient);
            return trameEncodee;
        }
    }
}
