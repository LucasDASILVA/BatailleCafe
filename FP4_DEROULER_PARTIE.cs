﻿using BatailleCafe.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe
{
    static class FP4_DEROULER_PARTIE
    {

        /*
        * FS4.1.3 INTERPRETER COORDONNEES
        * Transcrire la réponse et l'ordre en type coordonnées.
        * Entrée : ordre, une chaine contenant les coordonnées du client. reponse, une chaine contenant les coordonnées du serveur.
        *          coordOrdre, une référence de type Coordonnées. coordReponse, une référence de type Coordonnées.
        * Sortie : coordOrdre et coordReponse (modifiés par référence)
        */
        public static void InterpreterCoordonnees(string ordre, string reponse, ref typeCoord coordOrdre, ref typeCoord coordReponse )
        {
            coordOrdre.X = Int32.Parse(ordre.Substring(2, 1));
            coordOrdre.Y = Int32.Parse(ordre.Substring(3, 1));
            coordReponse.X = Int32.Parse(reponse.Substring(2, 1));
            coordReponse.Y = Int32.Parse(reponse.Substring(3, 1));

        }

        public static Dictionary<string,string> InterpreterScore(Dictionary<string,string> reponses)
        {
            string scoreClient = reponses["serveur"].Substring(2,2);
            string scoreServeur = reponses["serveur"].Substring(5, 2);


            reponses.Add("scoreClient", scoreClient);
            reponses.Add("scoreServeur", scoreServeur);
            return reponses;
        }

        /*
        * FS4.1.2 REPONSE_SERVEUR
        * Recevoir la réponse du serveur.
        * Entrée : socketClient, instance de classe Socket qui permet la communication avec le serveur
        * Sortie : reponses, un tableau de deux chaines contenant les réponses du serveur
        */
        public static Dictionary<string,string> ReponseServeur(Socket socketClient)
        {
            bool receptionScore = false;
            Dictionary<string, string> reponses = new Dictionary<string, string> { { "coupValide", "XXXX" }, { "serveur", "B:bb" }, {"etatPartie","XXXX" } };
            byte[] donnees;
            for(int index=0; index<reponses.Count();index++)
            {
                if (receptionScore == true) donnees = new byte[7];
                else donnees = new byte[4];
                
                try
                {
                    socketClient.Receive(donnees);
                }
                catch (Exception Erreur)
                {
                    Console.WriteLine(Erreur.ToString());
                    Console.ReadKey();
                }
                reponses[reponses.Keys.ElementAt(index)] = Encoding.ASCII.GetString(donnees);
                if (reponses[reponses.Keys.ElementAt(index)] == "FINI") receptionScore = true;
                
            }
            string score;
            if(reponses["serveur"] == "FINI")
            {
                score = reponses["etatPartie"];
                reponses["etatPartie"] = "FINI";
                reponses["serveur"] = score;
            }
            return reponses;

        }

        /*
        * FS4.1.1 ENVOI_SERVEUR
        * Envoyer une instruction au serveur.
        * Entrée : socketClient, instance de classe Socket qui permet la communication avec le serveur ; ordre, une chaine contenant l'instruction jouée par le client.
        * Sortie : Aucune
        */
        public static void EnvoiServeur(Socket socketClient, string ordre)
        {
            byte[] ordreEncode = Encoding.ASCII.GetBytes(ordre);
            socketClient.Send(ordreEncode);
        }


        /*
         * FS4.1 COMMUNICATION_SERVEUR
         * Assurer la communication entre le client et le serveur
         * Entrée : socketClient, instance de classe Socket qui permet la communication avec le serveur ; ordre, une chaine contenant l'instruction jouée par le client.
         * Sortie : reponse, une chaine contenant la réponse du serveur.
         */
        public static Dictionary<string,string> CommunicationServeur(Socket socketClient, string ordre, ref typeCoord coordOrdre, ref typeCoord coordReponse)
        {
            Dictionary<string, string> reponses;
            EnvoiServeur(socketClient, ordre);
            reponses = ReponseServeur(socketClient);
            InterpreterCoordonnees(ordre, reponses["serveur"], ref coordOrdre, ref coordReponse);
            if (reponses["etatPartie"] == "FINI")
            {
                reponses = InterpreterScore(reponses);
            }
            

            return reponses;
        }

        /*
         * FS4 DEROULER_PARTIE
         * Permettre le déroulement de la partie.
         * Entrée : carte, une instance de la classe Carte contenant les parcelles et unités. socketClient, instance de Classe Socket qui permet la communication avec le serveur.
         * Sortie : Aucune
         */
        public static void DeroulerPartie(Carte carte, Socket socketClient)
        {
            IntelligenceArtificielle client = new IntelligenceArtificielle(equipe.Client, 28);
            IntelligenceArtificielle serveur = new IntelligenceArtificielle(equipe.Serveur, 28);
            short tour = 1;
            bool continuer = true;
            string ordre = null;
            Dictionary<string, string> reponses;
            typeCoord dernierMouvement = new typeCoord(0, 0);
            bool premierTour = true;
            while(continuer == true)
            {
                //ordre = Programme.DEBUG();
                typeCoord coordOrdre = new typeCoord(0, 0);
                typeCoord coordReponse = new typeCoord(0,0);
                
                client.InitialisePriorite(dernierMouvement, carte, premierTour);
                premierTour = false;
                client.calculerPriorite(carte);
                Unite meilleureUnite = client.meilleurPriorite(carte);
                ordre = "A:" + meilleureUnite.GetCoordonnes().X + meilleureUnite.GetCoordonnes().Y;
                reponses = CommunicationServeur(socketClient, ordre, ref coordOrdre, ref coordReponse);
                dernierMouvement = coordReponse;
                if (reponses["coupValide"] == "VALI")
                {
                    client.PlacerGraine(coordOrdre, carte);
                    client.DecrementGraine();
                }
                else if (reponses["coupValide"] == "INVA")
                {
                    client.DecrementGraine();
                }
                

                if (reponses["etatPartie"] != "FINI")
                {
                    serveur.PlacerGraine(coordReponse, carte);
                    serveur.DecrementGraine();
                    Console.WriteLine("Client joue en {0} et Serveur joue en  {1}", ordre, reponses["serveur"]);
                    FP3_AFFICHER_CARTE.AfficherCarte(carte, tour);
                }

                if (reponses["etatPartie"] == "FINI")
                {
                    continuer = false;
                    FP3_AFFICHER_CARTE.AfficherFinPartie(carte, tour, reponses, ordre);
                }
                tour++;
            }
            
        }
    }
}
