using BatailleCafe.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;


namespace BatailleCafe
{

    static class FP2_DECODER_CARTE
    {

        static int NBCOLONNE = 10;
        static int NBLIGNE = 10;
        static int TAILLE = NBCOLONNE * NBLIGNE;

        /* FS2.1.1 : DECOMPOSER_TRAME
        * Extrait tous les codes d'unités de la trame séparément pour pouvoir ensuite les décoder une à une.
        * Entrée : trameCodee, une chaîne.
        *
        * Sortie : uniteCodees, un tableau d'entiers.
        */
        private static int[,] DecomposerTrame(string trameCodee)
        {
            int[,] uniteCodees = new int[NBCOLONNE,NBLIGNE];
            string[] tableauChaine = new string[TAILLE];
            trameCodee = trameCodee.Replace('|', ':');
            tableauChaine = trameCodee.Split(':');
            int tampon = 0;
            for (int indiceLigne = 0; indiceLigne < NBLIGNE; indiceLigne++)
            {
               for(int indiceColonne = 0; indiceColonne < NBCOLONNE; indiceColonne++)
                {
                    uniteCodees[indiceLigne, indiceColonne] = int.Parse(tableauChaine[tampon]);
                    tampon += 1;
                }
            }

            
            return uniteCodees;
        }

        /* FS2.1.2 : INSTANCIER_UNITE
        * Décoder les valeurs de la trame et instancier son Unité.
        * Entrée : uniteCodees, un tableau d'entier à 2 dimensions.
        *
        * Sortie : uniteDecodees, un tableau d'instance de classe Unite à 2 dimensions.
        */
        private static Unite[,] InstancierUnite(int[,] uniteCodees)
        {
            Unite[,] uniteDecodees = new Unite[NBLIGNE, NBCOLONNE];
            
            for (int indiceLigne = 0; indiceLigne < NBLIGNE; indiceLigne++)
            {
                for (int indiceColonne = 0; indiceColonne < NBCOLONNE; indiceColonne++)
                {
                    zone typeZone = zone.UniteVide;
                    bool fNord = false, fOuest = false, fSud = false, fEst = false;
                    char nomParcelle = ' ';
                    int uniteValeur = uniteCodees[indiceLigne, indiceColonne];
                    if (uniteValeur >= 64)
                    {
                        typeZone = zone.Mer;
                        nomParcelle = 'M';
                        uniteValeur = uniteValeur - 64;
                    }
                    if (uniteValeur >= 32)
                    {
                        typeZone = zone.Foret;
                        nomParcelle = 'F';
                        uniteValeur = uniteValeur - 32;
                    }
                    if (uniteValeur >= 8)
                    {
                        fEst = true;
                        uniteValeur = uniteValeur - 8;
                    }
                    if (uniteValeur >= 4)
                    {
                        fSud = true;
                        uniteValeur = uniteValeur - 4;
                    }
                    if (uniteValeur >= 2)
                    {
                        fOuest = true;
                        uniteValeur = uniteValeur - 2;
                    }
                    if (uniteValeur >= 1)
                    {
                        fNord = true;
                        uniteValeur = uniteValeur - 1;
                    }
                    uniteDecodees[indiceLigne, indiceColonne] = new Unite(new typeFrontiere(fNord, fSud, fOuest, fEst), typeZone, new typeCoord(indiceLigne, indiceColonne),nomParcelle);
                }
            }
            return uniteDecodees;
        }

        /* FS2.1.3 : ATTRIBUER_NOMPARCELLE
        * Attribuer à chaque unité sa parcelle correspondante.
        * Entrée : nomParcelle un caractère, tab, une référence vers un tableau d'instance d'Unités à 2 dimensions, indiceL et indiceC 2 entiers;
        *
        * Sortie : l'unité sélectionnée possède désormais un nom de parcelle.
        */
        private static void AttribuerNomParcelle(char nom, ref Unite[,] tab, int indiceL, int indiceC)
        {
            Unite uniteSelectionnee = tab[indiceL, indiceC];
            uniteSelectionnee.SetNomParcelle(nom);
            
        if( (!uniteSelectionnee.GetFrontieres().frontiereNord) && (tab[indiceL-1,indiceC].GetNomParcelle() == ' ') && (indiceL != 0))
        {
            AttribuerNomParcelle(nom, ref tab, indiceL - 1, indiceC);
        }
        if (!uniteSelectionnee.GetFrontieres().frontiereOuest && (tab[indiceL, indiceC - 1].GetNomParcelle() == ' ') && (indiceC != 0))
        {
            AttribuerNomParcelle(nom, ref tab, indiceL, indiceC - 1);
        }
        if (!uniteSelectionnee.GetFrontieres().frontiereSud)
        {
            AttribuerNomParcelle(nom, ref tab, indiceL + 1, indiceC);
        }
        if (!uniteSelectionnee.GetFrontieres().frontiereEst)
        {
            AttribuerNomParcelle(nom, ref tab, indiceL, indiceC + 1);
        }
            
            
        }

        /* FS2.1.4 : INSTANCIER_PARCELLE
         * Regroupe toutes les unités dans leur parcelle respective.
         * Entrée : uniteDecodees, un tableau d'instance de classe Unite à 2 dimensions.
         * Sortie : parcelles, un tableau d'instance de classe Passerelle.
        */
        private static Parcelle[] InstancierParcelles(Unite[,] unites)
        {
            // Etape 1 : Creation des 17 parcelles avec leur nom.
            char nomParcelle = 'a';
            Parcelle[] parcelles = new Parcelle[17];
            for(int index = 0; index < 17; index++)
            {
                parcelles[index] = new Parcelle(nomParcelle);
                if (nomParcelle != 'q') nomParcelle++;
            }
            // Etape 2 : Lie les unités à leur passerelle respective.
            foreach(Unite unite in unites)
            {
                foreach(Parcelle parcelle in parcelles)
                {
                    if(unite.GetNomParcelle() == parcelle.GetNomParcelle())
                    {
                        parcelle.addUnite(unite);
                    }
                }
            }
            // Etape 3 : Mettre à jour la répartition de chaque passerelle. (Pour le moment, toutes les unités n'ont pas encore de propriétaire).
            foreach(Parcelle parcelle in parcelles)
            {
                parcelle.UpdateRepartition();
            }
            
            return parcelles;
        }


        /* FS2.1 : DECODER_UNITE
        * Décompose la trame codée pour séparer les unités codées entre-elles puis les décode pour connaître leurs caractéristiques.
        * Entrée : trameCodee, une chaîne.
        *
        * Sortie : uniteDecodees, un tableau d'instance de classe Unite à 2 dimensions.
        */
        private static Unite[,] DecoderUnite(string trameCodee)
        {
            int[,] uniteCodees = new int[NBLIGNE, NBCOLONNE];
            Unite[,] uniteDecodees = new Unite[NBLIGNE,NBCOLONNE];
            uniteCodees = DecomposerTrame(trameCodee);
            uniteDecodees = InstancierUnite(uniteCodees);
            char nom = 'a';
            for (int indiceLigne = 0; indiceLigne < NBLIGNE; indiceLigne++)
            {
                for (int indiceColonne = 0; indiceColonne < NBCOLONNE; indiceColonne++)
                {
                    if(uniteDecodees[indiceLigne,indiceColonne].GetNomParcelle() == ' ')
                    {
                        AttribuerNomParcelle(nom, ref uniteDecodees, indiceLigne, indiceColonne);
                        nom++;
                    }
                }
            }
            return uniteDecodees;
        }

        /* FP2 : DECODER_CARTE
        * Découpe la trame codée pour obtenir un tableau des unités codées, décode une par une chacune des unités et les insère dans l'instance carte.
        * Entrées : trameCodee, une chaine; refCarte une référence d'instance Carte.
        *
        * Sortie : carte, une instance de Carte.
        */
        public static Carte DecoderCarte(string trameCodee)
        {
            Unite[,] uniteDecodees = new Unite[NBLIGNE, NBCOLONNE];
            uniteDecodees = DecoderUnite(trameCodee);
            Parcelle[] parcelles = InstancierParcelles(uniteDecodees);
            Carte carte = new Carte(uniteDecodees, parcelles);
            
            return carte;


        }



    }
}
