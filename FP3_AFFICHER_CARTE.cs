﻿using BatailleCafe.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Console = Colorful.Console;

namespace BatailleCafe
{

    /* FP3 : AFFICHER_CARTE
        * Affiche le contenu de la carte à savoir toutes les unités en fonction de leurs coordonnées.
        * Entrée : carte, une instance de la classe Carte;
        *
        * Sortie : Aucun (Affichage du contenu);
        */
    static class FP3_AFFICHER_CARTE
    {
        public static void AfficherCarte(Carte carte, short tour)
        {
            Console.WriteLine("==============================================");
            Console.WriteLine("Code couleur : ");
            Console.Write("Client : ■ ", Color.Green);
            Console.Write(" | ");
            Console.WriteLine("Serveur : ■", Color.Red);
            carte.Affichage();
            Console.WriteLine("Tour numero : {0}",tour, Color.Orange);
            Console.WriteLine("==============================================");
        }

        public static void AfficherFinPartie(Carte carte, short tour,Dictionary<string,string> reponses, string ordre)
        {
            Console.WriteLine("Client conclut par le placement en {0}", ordre);
            FP3_AFFICHER_CARTE.AfficherCarte(carte, tour);
            
            Console.WriteLine("==============================================");
            Console.WriteLine("PARTIE TERMINEE !");
            Console.WriteLine("Client : {0} points", reponses["scoreClient"], Color.Green);
            Console.WriteLine("Serveur : {0} points", reponses["scoreServeur"], Color.Red);
            if (int.Parse(reponses["scoreClient"]) > int.Parse(reponses["scoreServeur"]))
            {
                Console.WriteLine("L'IA client a rappelé qui était le patron...");
            }
            else Console.WriteLine("L'IA serveur a eu beaucoup de chance, on était pas en forme aujourd'hui ...");
            Console.WriteLine("==============================================");
        }
    }
}
