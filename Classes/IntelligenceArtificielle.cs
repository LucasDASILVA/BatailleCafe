﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe.Classes
{
    enum Regles
    {
        // Enumeration des points de priorité etablis en fonction des règles de l'IA.

        // REGLE IMPAIR :  Favoriser les parcelles de 3 et de 5
        // ==> N'est plus valide lorsque la parcelle est déjà sous le contrôle d'un joueur (controle = true)
        uniteImpaires = 5,
        // REGLE DOMINATION : Jouer sur cette case entraînerait la domination d'une parcelle de 6/5/4/3/2
        // ==> N'est plus valide lorsque la parcelle est déjà sous le contrôle d'un joueur (controle = true)
        coupDominantSix = 15,
        coupDominantCinq = 8, // 8 + 5 = 13 (impair)
        coupDominantQuatre = 10,
        coupDominantTrois = 3, // 3 + 5 = 8 (impair)
        coupDominantDeux = 5,
        // REGLE EGALISATION : Egaliser sur une parcelle de 6/4/2 unités
        egaliserSix = 20,
        egaliserQuatre = 15,
        egaliserDeux = 8,
        // REGLE D'ATTAQUE : Faire obstacle au serveur en plantant les graines sur ses territoires.
        attaqueParUniteEnnemie = 5,
        // REGLE D'ETENDUE : Graine voisine alliée (Objectif, favoriser le regroupement des unités)
        uniteVoisine = 3
        // REGLE DU GROUPE : Favoriser le placement d'unités dans des parcelles possédant déjà nos unités. +2 points par unité alliée.
        // ==> Pas nécessaire de l'énumérer puisqu'il faut juste rajouter le nombre d'unités alliés dans la passerelle * 2.
        
    }
    class IntelligenceArtificielle
    {
        private int nombreGraine;
        private equipe joueur;
        private int score;

        public IntelligenceArtificielle(equipe joueur, int nombreGraine)
        {
            this.joueur = joueur;
            this.nombreGraine = nombreGraine;
        }

        public void PlacerGraine(typeCoord coord, Carte carte)
        {
            carte.placementGraine(coord, joueur);
            carte.UpdateRepartitionParcelles();
            Parcelle[] parcelles = carte.GetParcelles();
            foreach(Parcelle parcelle in parcelles)
            {
                if (parcelle.GetNomParcelle() == 'b')
                {
                    float index = (float)parcelle.GetRepartition().nombreUniteClient / parcelle.GetNombreUnite();
                }
                if( ((float)parcelle.GetRepartition().nombreUniteClient / parcelle.GetNombreUnite() > 0.5) && parcelle.getControle() == false)
                {
                    parcelle.setControle(true);
                }
                if (((float)parcelle.GetRepartition().nombreUniteClient / parcelle.GetNombreUnite() == 0.5) && parcelle.getControle() == false)
                {
                    parcelle.setControle(true);
                }
            }
            

        }

        public void TesterPlacement(Unite unite, Carte carte, equipe joueur, zone type = zone.UniteOccupee)
        {
            carte.placementGraine(unite.GetCoordonnes(), joueur, type);
            carte.UpdateRepartitionParcelles();
        }


        public void DecrementGraine()
        {
            nombreGraine--;
        }

        public int GetNombreGraine()
        {
            return nombreGraine;
        }

        public equipe GetEquipeJoueur()
        {
            return joueur;
        }


        public void calculerPriorite(Carte carte)
        {
            foreach (Unite unite in carte.getContenu())
            {
                if(unite.GetTypeUnite() == zone.UniteVide)
                {
                    Parcelle[] parcelles = carte.GetParcelles();
                    foreach (Parcelle parcelle in parcelles)
                    {
                        if (parcelle.GetNomParcelle() == unite.GetNomParcelle())
                        {
                            TesterPlacement(unite, carte, equipe.Client); // Permet juste la simulation pour etablir le meilleur placement.
                            RegleImpair(parcelle, unite);
                            RegleDomination(parcelle, unite);
                            RegleAttaque(carte);
                            RegleEgalisation(parcelle, unite);
                            RegleEtendue(carte, parcelle, unite);
                            TesterPlacement(unite, carte, equipe.Aucun, zone.UniteVide);
                        }
                    }
                }
                
            }
        }

        public Unite meilleurPriorite(Carte carte)
        {
            short prioriteMax = 0;
            Unite meilleureUnite = null;
            foreach (Unite unite in carte.getContenu())
            {
                if(unite.GetPriorite() > prioriteMax)
                {
                    prioriteMax = unite.GetPriorite();
                    meilleureUnite = unite;
                }
                if (prioriteMax == 0)
                {
                    meilleureUnite = unite;
                }
                
            }
            return meilleureUnite;
        }

        public void InitialisePriorite(typeCoord dernierMouvement, Carte carte, bool premierTour)
        {
            Unite[,] contenu = carte.getContenu();
            char parcelleBloquee = contenu[dernierMouvement.X, dernierMouvement.Y].GetNomParcelle();
            foreach (Unite unite in contenu)
            {
                if (unite.GetTypeUnite() == zone.UniteVide)
                {
                   
                    // Toutes les unités qui ne sont pas sur la même ligne ni sur la même colonne ou qui appartiennent à la parcelle bloquée subissent le dédain de l'IA
                    if (( (unite.GetCoordonnes().X != dernierMouvement.X && unite.GetCoordonnes().Y != dernierMouvement.Y) || unite.GetNomParcelle() == parcelleBloquee) && premierTour == false)
                    {
                        unite.SetPriorite(-9999);
                    }
                    else unite.SetPriorite(0); // Enfin, si l'IA peut jouer sur cette unité, sa priorité est initialisée à 0.

               
                }
                else unite.SetPriorite(-9999); // Si l'unité est une forêt/mer/unité déjà occupée. On s'assure que l'IA ne voudra jamais jouer dessus.
            }
        }


        private void RegleImpair(Parcelle parcelle, Unite unite)
        {
            if( (parcelle.GetNombreUnite() == 3 || parcelle.GetNombreUnite() == 5) && parcelle.getControle() == false)
            {
                unite.addPriorite((short)Regles.uniteImpaires);
            }
        }

        private void RegleDomination(Parcelle parcelle, Unite unite)
        {
            if(((double)parcelle.GetRepartition().nombreUniteClient / parcelle.GetNombreUnite() > 0.5) && parcelle.getControle() == false ) // Si on possède plus de la moitié de la parcelle
            {
                switch (parcelle.GetNombreUnite())
                {
                    case 6:
                        unite.addPriorite((short)Regles.coupDominantSix);
                        break;
                    case 5:
                        unite.addPriorite((short)Regles.coupDominantCinq);
                        break;
                    case 4:
                        unite.addPriorite((short)Regles.coupDominantQuatre);
                        break;
                    case 3:
                        unite.addPriorite((short)Regles.coupDominantTrois);
                        break;
                    case 2:
                        unite.addPriorite((short)Regles.coupDominantDeux);
                        break;
                    
                }
            }
        }

        private void RegleEgalisation(Parcelle parcelle, Unite unite)
        {
            if (((double)parcelle.GetRepartition().nombreUniteClient / parcelle.GetNombreUnite() == 0.5) && parcelle.GetRepartition().nombreUniteServeur > 0) // Si on a la possibilité de bloquer le serveur
            {
                switch (parcelle.GetNombreUnite())
                {
                    case 6:
                        unite.addPriorite((short)Regles.egaliserSix);
                        break;
                    case 4:
                        unite.addPriorite((short)Regles.egaliserQuatre);
                        break;
                    case 2:
                        unite.addPriorite((short)Regles.egaliserDeux);
                        break;
                }
            }
        }

        private void RegleAttaque(Carte carte)
        {
            Parcelle[] parcelles = carte.GetParcelles();
            foreach(Parcelle parcelle in parcelles)
            {
                List<Unite> unitesParcelles = parcelle.getListeUnites();
                foreach(Unite unite in unitesParcelles)
                {
                    int points = Convert.ToInt32(Regles.attaqueParUniteEnnemie)*parcelle.GetRepartition().nombreUniteServeur;
                    unite.addPriorite(Convert.ToInt16(points));
                }
            }
        }

        private void RegleEtendue(Carte carte,Parcelle parcelle, Unite unite)
        {
            typeCoord coordonnes = unite.GetCoordonnes();
            Unite[,] contenu = carte.getContenu();
            // Redondance nécessaire puisqu'il faut que les points de priorité s'accumulent (Si l'unite a un voisin des deux côtés par exemple)
            if( (coordonnes.X != 0) && (coordonnes.X != 9) )
            {
                if (contenu[coordonnes.X + 1, coordonnes.Y].GetProprietaire() == equipe.Client)
                {
                    unite.addPriorite((short)Regles.uniteVoisine);
                }
                if (contenu[coordonnes.X - 1, coordonnes.Y].GetProprietaire() == equipe.Client)
                {
                    unite.addPriorite((short)Regles.uniteVoisine);
                }
            }
            if((coordonnes.Y != 0) && (coordonnes.Y != 9))
            {
                if (contenu[coordonnes.X, coordonnes.Y + 1].GetProprietaire() == equipe.Client)
                {
                    unite.addPriorite((short)Regles.uniteVoisine);
                }
                if (contenu[coordonnes.X, coordonnes.Y - 1].GetProprietaire() == equipe.Client)
                {
                    unite.addPriorite((short)Regles.uniteVoisine);
                }
            }
            
            // Regle du groupe : +2 points par allié dans la parcelle
            short pointsBonus = Convert.ToInt16( (parcelle.GetRepartition().nombreUniteClient - 1) * 2); // On enlève 1 pour ne pas prendre encompte la graine "simulée"
            unite.addPriorite(pointsBonus);
        }
    }
}
