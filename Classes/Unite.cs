﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe.Classes
{
    struct typeFrontiere
    {
        public bool frontiereNord;
        public bool frontiereSud;
        public bool frontiereOuest;
        public bool frontiereEst;

        public typeFrontiere(bool frNord,bool frSud, bool frOuest, bool frEst)
        {
            frontiereNord = frNord;
            frontiereSud = frSud;
            frontiereOuest = frOuest;
            frontiereEst = frEst;
        }
    }

    enum zone
    {
        Mer = 64,
        Foret = 32,
        UniteVide = 0,
        UniteOccupee = 1

    }

    enum equipe
    {
        Client = 0,
        Serveur = 1,
        Aucun = -1
    }

    struct typeCoord
    {
        public int X;
        public int Y;

        public typeCoord(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }
    }

    class Unite
    {
        private typeFrontiere frontieres;
        private zone typeUnite;
        private equipe proprietaire;
        private typeCoord coordonnees;
        private char nomParcelle;
        private short priorite;

        public Unite(typeFrontiere frontieres, zone typeUnite, typeCoord coordonnees, char nomParcelle)
        {
            this.frontieres = frontieres;
            this.typeUnite = typeUnite;
            this.coordonnees = coordonnees;
            this.nomParcelle = nomParcelle;
            this.proprietaire = equipe.Aucun;
            this.priorite = 0;
        }

        public typeFrontiere GetFrontieres()
        {
            return this.frontieres;
        }

        public zone GetTypeUnite()
        {
            return this.typeUnite;
        }

        public void SetTypeUnite(zone typeUnite)
        {
            this.typeUnite = typeUnite;
        }

        public typeCoord GetCoordonnes()
        {
            return this.coordonnees;
        }

        public void SetNomParcelle(char nom)
        {
            nomParcelle = nom;
        }

        public char GetNomParcelle()
        {
            return this.nomParcelle;
        }

        public void SetPriorite(short priorite)
        {
            this.priorite = priorite;
        }

        public short GetPriorite()
        {
            return this.priorite;
        }

        public void SetProprietaire(equipe proprietaire)
        {
            this.proprietaire = proprietaire;
        }

        public equipe GetProprietaire()
        {
            return this.proprietaire;
        }

        public void addPriorite(short priorite)
        {
            this.priorite += priorite;
        }


    }
}
