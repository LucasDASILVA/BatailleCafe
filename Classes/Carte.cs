﻿using System.Drawing;
using Console = Colorful.Console;

namespace BatailleCafe.Classes
{
    class Carte
    {
        static int NBCOLONNE = 10;
        static int NBLIGNE = 10;
        private Parcelle[] parcelles;
        private Unite[,] contenu;

        public Carte(Unite[,] contenu, Parcelle[] parcelles)
        {
            this.contenu = contenu;
            this.parcelles = parcelles;
        }

        public Unite[,] getContenu()
        {
            return this.contenu;
        }

        public void setContenu(Unite[,] contenu)
        {
            this.contenu = contenu;
        }

        public void placementGraine(typeCoord coord, equipe proprio, zone type = zone.UniteOccupee)
        {
            this.contenu[coord.X, coord.Y].SetProprietaire(proprio);
            this.contenu[coord.X, coord.Y].SetTypeUnite(type);
        }

        public void UpdateRepartitionParcelles()
        {
            foreach(Parcelle parcelle in parcelles)
            {
                parcelle.UpdateRepartition();
            }
        }

       

        public void Affichage()
        {
            for (int indiceLigne = 0; indiceLigne < NBLIGNE; indiceLigne++)
            {
                for (int indiceColonne = 0; indiceColonne < NBCOLONNE; indiceColonne++)
                {
                    if (contenu[indiceLigne, indiceColonne].GetProprietaire() == equipe.Client)
                    {
                        Console.Write("{0} ", contenu[indiceLigne, indiceColonne].GetNomParcelle(), Color.Green);
                    }
                    else if (contenu[indiceLigne, indiceColonne].GetProprietaire() == equipe.Serveur)
                    {
                        Console.Write("{0} ", contenu[indiceLigne, indiceColonne].GetNomParcelle(), Color.Red);
                    }
                    else Console.Write("{0} ", contenu[indiceLigne, indiceColonne].GetNomParcelle());

                }
                Console.WriteLine();
            }
        }



        public Parcelle[] GetParcelles()
        {
            return parcelles;
        }


    }
}
