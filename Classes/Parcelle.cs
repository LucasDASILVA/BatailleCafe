﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe.Classes
{
    struct typeRepartition
    {
        public int nombreUniteClient;
        public int nombreUniteServeur;
        public int nombreUniteVide;
        
    }
    class Parcelle
    {
        char nomParcelle;
        int nombreUnite;
        List<Unite> unitesDansParcelle;
        typeRepartition repartition;
        bool controle;

        public Parcelle(char nomParcelle, int nombreUnite, List<Unite> unitesDansParcelle, typeRepartition repartition,bool controle = false)
        {
            this.nomParcelle = nomParcelle;
            this.nombreUnite = nombreUnite;
            this.unitesDansParcelle = unitesDansParcelle;
            this.repartition = repartition;
        }

        public Parcelle(char nomParcelle)
        {
            this.nomParcelle = nomParcelle;
            this.nombreUnite = 0;
            this.unitesDansParcelle = new List<Unite>();
            this.repartition.nombreUniteVide = 0;
            this.repartition.nombreUniteClient = 0;
            this.repartition.nombreUniteServeur = 0;
            this.controle = false;
        }

        public char GetNomParcelle()
        {
            return nomParcelle;
        }

        public int GetNombreUnite()
        {
            return nombreUnite;
        }

        public typeRepartition GetRepartition()
        {
            return repartition;
        }

        public List<Unite> getListeUnites()
        {
            return this.unitesDansParcelle;
        }

        public void addUnite(Unite unite)
        {
            this.unitesDansParcelle.Add(unite);
            this.nombreUnite = this.unitesDansParcelle.Count();
        }

        public void setControle(bool etat)
        {
            this.controle = etat;
        }

        public bool getControle()
        {
            return this.controle;
        }

        public void UpdateRepartition()
        {
            repartition.nombreUniteClient = 0;
            repartition.nombreUniteServeur = 0;
            repartition.nombreUniteVide = 0;
            foreach(Unite unite in unitesDansParcelle)
            {
                if(unite.GetProprietaire() == equipe.Aucun)
                {
                    this.repartition.nombreUniteVide++;
                }
                else if (unite.GetProprietaire() == equipe.Client)
                {
                    this.repartition.nombreUniteClient++;
                }
                else
                {
                    this.repartition.nombreUniteServeur++;
                }
            }
        }
    }
}
