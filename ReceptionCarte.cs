﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace InitialisationCarte
{
    class ReceptionCarte
    {
        public static string recevoirCarte(Socket socketClient)
        {
            string carteEncodee;
            byte[] donnees = new byte[300];
            
            try
            {
                socketClient.Connect("51.91.120.237", 1212);
                socketClient.Receive(donnees);
            }
            catch (Exception Erreur)
            {
                Console.WriteLine(Erreur.ToString());
                Console.ReadKey();
            }
            carteEncodee = Encoding.ASCII.GetString(donnees);
            socketClient.Shutdown(SocketShutdown.Receive);
            socketClient.Close();
            return carteEncodee;
        }

        static void Main(string[] args)
        {
            string carteDecodee;
            Socket socketClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            carteDecodee = recevoirCarte(socketClient);
            Console.WriteLine(carteDecodee);
        }
    }
}
