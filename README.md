﻿# ![](static/img/LogoProjet.png)

Jeu intitulé "La bataille du café" dans le cadre d'un projet en C# de 2ème année au DUT Informatique d'Amiens.

## Equipe NoBrainNoGain

* Charles HENNEBERT (Groupe 3)
* Gabriel SALVETTI (Groupe 3)
* Lucas DA SILVA (Groupe 3)
* Mattéo RICHARD (Groupe 2)
